# apt-verify CONTRIBUTING

We welcome contributions to `apt-verify`.

Contributor must certify the [Developer Certificate of
Origin](https://developercertificate.org/) for their contribution by
adding the Signed-off-by: tag to their submission.
