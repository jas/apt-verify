# apt-verify NEWS

[TOC]

## Noteworthy changes in release 2.0 (2023-12-22) [beta]

Rename script apt-verify-gpgv to apt-verify.

The rationale is to use a shorter and easier to read scriptname.
https://gitlab.com/debdistutils/apt-verify/-/issues/2

## Noteworthy changes in release 1.2 (2023-04-09) [beta]

Documentation fixes.

## Noteworthy changes in release 1.1 (2023-04-09) [beta]

Typo fix in README.

## Noteworthy changes in release 1.0 (2023-04-09) [beta]

Initial release.
