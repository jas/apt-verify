% apt-verify(8) | System administration commands

NAME
====

apt-verify - allow extension of apt's gpgv-based verification mechanism

SYNOPSIS
========

`apt-verify [options]`

DESCRIPTION
===========

`apt-verify` is only intended to be called internally by `apt(8)` via
`apt-key(8)`, replacing its direct use of `gpgv(1)`.  It works by
enumerating and invoking all tools in `/etc/apt/verify.d/`.  Normally
one of the tools should be the traditional `gpgv(1)`.

When configured, `apt(8)` will verify downloaded "release" files by
calling out to the [apt-verify](apt-verify) script, replacing the call
from `apt-key verify` to `gpgv`.

Test it by running `apt-get update` and you should see something like
the following in `/var/log/syslog`:

```
Feb  8 12:32:08 kaka apt-verify: /etc/apt/verify.d/gpgv --homedir /tmp/apt-key-gpghome.i8sOBsTZA5 --keyring /tmp/apt-key-gpghome.i8sOBsTZA5/pubring.gpg --ignore-time-conflict --status-fd 3 /tmp/apt.sig.35yprB /tmp/apt.data.9zW8zG
```

This is how `apt-key verify` called out to `apt-verify`.  These are
debug messages, so you may wish to filter them out.

CONFIGURATION
=============

Normally the configuration happens during system installation of this
tool.  To configure `apt(8)` to use `apt-verify(8)`, you may configure
it as follows:

```
# echo 'APT::Key::gpgvcommand "apt-verify";' > /etc/apt/apt.conf.d/75verify
```

To confirm the configuration, you may use `apt-config(8)` as follows:

```
$ apt-config dump|grep APT::Key::gpgvcommand
APT::Key::gpgvcommand "apt-verify";
$
```


FILES
=====

`/etc/apt/verify.d`

The directory `/etc/apt/verify.d` is used by `apt-verify(8)`.

Normally this directory contains symbolic links to executable files,
to make it easy for sysadmins to enable (ln -s) and disable (rm) each
tool, but it is permitted to put a executable directly in this
directory.

Programs are executed ordered by a sorted name, and if one program
fails, the remaining programs will never be invoked.  Non-executable
files such as this README are ignored.

Apt parses GnuPG `gpgv(1)` outputs for signature verification status,
so you want to have a symbolic link `gpgv` in this directory pointing
to `/usr/bin/gpgv` or whatever `gpgv` you wish to use.

Apt ignores the exit code of these processes and only parse any GnuPG
`gpgv` outputs.  So if you place a program whose name sort after
`gpgv`, exiting with non-zero is not sufficient to cause apt to fail.

For the above reasons, we suggest placing any non-gpgv programs you
wish to be run under symbolic links prefixed with digits, like this:

```
$ ls -la /etc/apt/verify.d/
total 16
drwxr-xr-x  2 root root 4096 Feb  7 17:57 .
drwxr-xr-x 10 root root 4096 Feb  7 17:24 ..
-rwxr-xr-x  1 jas  jas  2065 Feb  7 17:57 10canary
-rw-r--r--  1 root root 1385 Feb  7 17:57 README
lrwxrwxrwx  1 root root   13 Feb  7 17:24 gpgv -> /usr/bin/gpgv
$
```

OPTIONS
=======

Any options are passed on to the scripts in `/etc/apt/verify.d`
without any parsing or checking.

DIAGNOSTICS
===========

`apt-verify` returns zero on normal operation, but if any of the
scripts in `/etc/apt/verify.d` return an error code, the same error
code is returned.

SEE ALSO
========

apt(8), apt.conf(5), gpgv(1)

https://gitlab.com/debdistutils/apt-verify
